﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
namespace TileEngine
{
    public class FrameAnimation : ICloneable
    {
        //Array of Rectangles to describe frames.
        Rectangle[] frames;
        //Int to tell what frame is being displayed.
        int currentFrame = 0;
        //Number of sceonds to wait before changing to the next frame.
        float frameLength = 0.5f;
        //Float to tell how much time has passed.
        float timer = 0f;

        //Value to set or get the frames per second.
        public int FramesPerSecond
        {
            get
            {
                return (int)(1f /  frameLength);
            }

            set
            {
                frameLength = (float)Math.Max(1f / (float) value , 0.001f);
            }
        }

        //Accessor to get the rectangle of the current frame.
        public Rectangle CurrentRect
        {
            get { return frames[currentFrame]; }

        }
        //Accessor to see what frame the animation is currently on/
        public int CurrentFrame
        {
            get { return currentFrame; }
            set
            {
                //Ensuring the value passed in is within the bounds of the frames array's indices.
                currentFrame = (int)MathHelper.Clamp(value, 0, frames.Length - 1);
            }
        }

        public void Update(GameTime gameTime)
        {
            timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if(timer >= frameLength)
            {
                timer = 0f;
                currentFrame = (currentFrame + 1) % frames.Length;

            }
        }
        //
        public object Clone()
        {
            FrameAnimation anim = new FrameAnimation();
            anim.frameLength = frameLength;
            anim.frames = frames;
            return anim;
        }

        private FrameAnimation()
        {

        }


        //Constructor for the FrameAnimation.
        public FrameAnimation(int numberOfFrames, int frameWidth, int frameHeight,int xOffset,int yOffset)
        {
            frames = new Rectangle[numberOfFrames];
            //For each frame specified
            for(int i = 0; i<numberOfFrames; i++)
            {
                //Create a new Rectangle and set its width and height to the values passed in.
                Rectangle rect = new Rectangle();
                rect.Width = frameWidth;
                rect.Height = frameHeight;

                //Set its x and y based on the x and y offset specified.
                rect.X = xOffset + (i * frameWidth);
                rect.Y = yOffset;
                //add rect to the the frame array.
                frames[i] = rect;

            }

        }

    }
}
