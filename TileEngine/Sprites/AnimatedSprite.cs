﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileEngine
{
    public class AnimatedSprite
    {
        //Dictionary to store our animations based on string key. where the string is a label describing what animation.
        public Dictionary<string, FrameAnimation> Animations = new Dictionary<string, FrameAnimation>();

        //string telling what animation is being used
        string currentAnimation = null;
        //bool to toggle the animation.
        bool animating = true;
        //Texture of the sprite.
        Texture2D texture;
        //Vector 2 to store position of the sprite.
        public Vector2 Position = Vector2.Zero;
        //Vector 2 to represent an offset from the origing of the sprite.
        public Vector2 OriginOffset = Vector2.Zero;

        //Acessor to get the Origin of the pixel taking into account the origin offset.
        public Vector2 Origin
        {
            get { return Position + OriginOffset; }
        }

        public Vector2 Center
        {
            get
            {
                return Position + new Vector2(
                    CurrentAnimation.CurrentRect.Width / 2,
                    CurrentAnimation.CurrentRect.Height / 2);
            }
        }

        public Rectangle Bounds
        {
            get
            {
                Rectangle rect = CurrentAnimation.CurrentRect;
                rect.X = (int)Position.X;
                rect.Y = (int)Position.Y;
                return rect;
            }
        }

        float radius = 8;
        float speed = 0.1f;

        public float Speed
        {
            get { return speed; }

            set
            {
                speed = (float)Math.Max(value, .1f);
            }
        }

        public float CollisionRadius
        {
            get { return radius; }
            set { radius = (float)MathHelper.Max(value, 1f); }
        }




        //Accessor to tell if the animation is running.
        public bool IsAnimating
        {
            get { return animating; }
            set { animating = value; }
        }
        //Accessor for the current FrameAnimation running.
        public FrameAnimation CurrentAnimation
        {
            get
            {
                if (!string.IsNullOrEmpty(currentAnimation))
                    return Animations[currentAnimation];
                else
                    return null;
            }

        }

        //Accessor for the current animation's name string.
        public string CurrentAnimationName
        {
            get { return currentAnimation; }
            set
            {
                if (Animations.ContainsKey(value))
                    currentAnimation = value;
            }

        }

        //constructor for the animated sprite.
        public AnimatedSprite(Texture2D texture)
        {
            this.texture = texture;
        }

        //Method to determine if 2 sprites are colliding.
        public static bool AreColliding(AnimatedSprite a, AnimatedSprite b)
        {
            Vector2 d = b.Origin - a.Origin;
            return (d.Length() < b.CollisionRadius + a.CollisionRadius);
        }

        public void ClampToArea(int width, int height)
        {
            if (Position.X < 0)
                Position.X = 0;
            if (Position.Y < 0)
                Position.Y = 0;
            if (Position.X > width - CurrentAnimation.CurrentRect.Width / 2)
                Position.X = width - CurrentAnimation.CurrentRect.Width / 2;
            if (Position.Y > height - CurrentAnimation.CurrentRect.Height / 2)
                Position.Y = height - CurrentAnimation.CurrentRect.Height / 2;
        }


        //method to update current animation.
        public  virtual void Update(GameTime gameTime)
        {
            //if the animation is not animating disregard the update.
            if (!IsAnimating)
            {
                return;
            }

            FrameAnimation  animation = CurrentAnimation;
            //Checking the animation is not null;
            if(animation == null)
            {
                //if an animation exists reset to first animation.
                if(Animations.Count > 0)
                {
                    string[] keys = new String[Animations.Count];
                    Animations.Keys.CopyTo(keys, 0);

                    currentAnimation = keys[0];
                    animation = CurrentAnimation;

                }
                else
                {
                    return;
                }
            }


            animation.Update(gameTime);
        }

        //Method for drawing sprite to screen.
        public void Draw(SpriteBatch spriteBatch)
        {
            FrameAnimation animation = CurrentAnimation;
            if (animation != null)
                spriteBatch.Draw(texture, Position, animation.CurrentRect, Color.White);
        }

    }
}
