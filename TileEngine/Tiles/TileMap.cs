﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace TileEngine
{
    //Class to represent an entire tile map.
    public class TileMap
    {
        public List<TileLayer> Layers = new List<TileLayer>();
        public CollisionLayer CollisionLayer;


        public void Draw(SpriteBatch spriteBatch, Camera camera)
        {
            

            foreach (TileLayer layer in Layers)
            {
                //Camera position is considered the topleft of the screen so the min bound = camera's position.
                Point min = Engine.ConvertPositionToCell(camera.Position);
                //Camera position max is the camera position + dimensions of the screen + the dimensions of a tile.
                //This is done to draw a little bit more than out size the screen so there is no pop in of tiles.

                Point max = Engine.ConvertPositionToCell(camera.Position+ new Vector2(
                    spriteBatch.GraphicsDevice.Viewport.Width + Engine.TileWidth,
                    spriteBatch.GraphicsDevice.Viewport.Height + Engine.TileHeight));

                layer.Draw(spriteBatch, camera, min, max);
            }
        }

       
        public float GetWidthInPixels()
        {
            return Layers[0].WidthInPixels;
        }

        public float GetHeightInPixels()
        {
            return Layers[0].HeightInPixels;
        }
    }

    
}
