﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
namespace TileEngine
{
    public static class Engine
    {
        //Int to store the width of a tile in pixels.
        public const int TileWidth = 32;
        //Int to store the height of a tile in pixels.
        public const int TileHeight = 32;

        public static Point ConvertPositionToCell(Vector2 position)
        {
            return new Point((int)(position.X / (float)TileWidth), (int)(position.Y / (float) TileHeight));
        }

        public static Rectangle CreateRectForCell(Point cell)
        {
            return new Rectangle(
                cell.X * TileWidth,
                cell.Y * TileHeight,
                TileWidth,
                TileHeight);
        }
    }
}
