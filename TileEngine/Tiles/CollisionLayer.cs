﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace TileEngine
{
    //Class to represent a single layer in a tile.
    public class CollisionLayer
    {
       


       

        //2D array of Ints to represent our map. The ints correspond to the index of a texture in the tileTextures list.
        int[,] map;
        


        public CollisionLayer(int width, int height)
        {
            map = new int[height, width];
        }

        public int Width
        {
            get { return map.GetLength(1); }
        }

        public int Height
        {
            get{ return map.GetLength(0); }
        }
       
        //Method to load a CollisionLayer from a file.
        public static CollisionLayer FromFile(ContentManager content, string fileName)
        {

            CollisionLayer CollisionLayer;
            bool readingLayout = false;
            List <List<int>> tempLayout = new List<List<int>>();

            using (StreamReader reader = new StreamReader(fileName))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine().Trim();
                    if (string.IsNullOrEmpty(line.Trim()))
                        continue;

                   
                    //If we hit the layout tag set the reading layout to true.
                    else if (line.Contains("[Layout]"))
                    {
                        readingLayout = true;
                    }
                 
                    //if we are reading layout...
                    else if (readingLayout)
                    {
                        //List to store the row. read in from the layout section of the file.
                        List<int> row = new List<int>();
                        string[] cells = line.Split(' ');
                        //double check theres no null character in cells array.
                        foreach(string c in cells)
                        {
                            if (!string.IsNullOrEmpty(c))
                                row.Add(int.Parse(c)); 
                        }
                        //Add the row to the temp layout.
                        tempLayout.Add(row);
                    }


                }
            }
            //Grab the width and height of the temp layout based on the size of its contents.
            int width = tempLayout[0].Count;
            int height = tempLayout.Count;
            //Create a new tile layer.
            CollisionLayer = new CollisionLayer(width, height);
            //Copy the information in the temp layout to the map of the layer.
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    CollisionLayer.SetCellIndex(x, y, tempLayout[y][x]);
                }
            }
           


            return CollisionLayer;
        }

        public int getCellIndex(int x, int y)
        {
           return map[y, x];
        }

        public int GetCellIndex(Point point)
        {
            Point check = point;
            if(check.X >= Width)
            {
                check.X = Width-1;
            }

            if(check.Y >= Height)
            {
                check.Y = Height - 1;
            }
            return map[check.Y, check.X];
        }


        public void SetCellIndex(Point point, int cellIndex)
        {
            map[point.Y, point.X] = cellIndex;
        }

        public void SetCellIndex(int x, int y, int cellIndex)
        {
            map[y, x] = cellIndex;
        }
    

       
    }
}
