﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace TileEngine
{
    //Class to represent a single layer in a tile.
    public class TileLayer
    {
       


        //A list of Textures to store our tiles into.
        List<Texture2D> tileTextures = new List<Texture2D>();

        //2D array of Ints to represent our map. The ints correspond to the index of a texture in the tileTextures list.
        int[,] map;
        //Float to represent the transparency of the layer 0 transparent - 1f opaque.
        float alpha = 1f;
        //Accessor of the alpha of the layer.
        public float Alpha
        {
            get { return alpha; }
            set { alpha = MathHelper.Clamp(value, 0f, 1f); }
        }
        //Accessor for the width of the layer
        public int Width
        {
            get { return map.GetLength(1); }
        }
        //Accessor for the height of the layer.
        public int Height
        {
            get { return map.GetLength(0); }
        }
        //Acccessor for the width of the layer in Pixels.
        public int WidthInPixels
        {
            get { return map.GetLength(1) * Engine.TileWidth; }
        }
        //Accessor for the height of the layer in pixels.
        public int HeightInPixels
        {
            get { return map.GetLength(0) * Engine.TileHeight; }
        }

        //Basic constructor for the TileLayer 
        public TileLayer(int width, int height)
        {
            map = new int[height, width];
        }
        //Copy constructor for a TileLayer given a map array passsed in. 
        public TileLayer(int[,] existingMap)
        {
            map = (int[,])existingMap.Clone();

        }
        //Static Method to load a tileLayer from a file.
        public static TileLayer FromFile(ContentManager content, string fileName)
        {

            TileLayer tileLayer;
            bool readingTextures = false;
            bool readingLayout = false;
            List<String> textureNames = new List<String>();
            List <List<int>> tempLayout = new List<List<int>>();

            using (StreamReader reader = new StreamReader(fileName))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine().Trim();
                    if (string.IsNullOrEmpty(line.Trim()))
                        continue;

                    //If we hit the Textures tag then set reading textures to true.
                    if (line.Contains("[Textures]"))
                    {
                        readingTextures = true;
                        readingLayout = false;

                    }
                    //If we hit the layout tag set the reading layout to true.
                    else if (line.Contains("[Layout]"))
                    {
                        readingLayout = true;
                        readingTextures = false;
                    }
                    //If we are reading textures then add the line to the texture name list.
                    else if (readingTextures)
                    {
                        textureNames.Add(line); 
                    }
                    //if we are reading layout...
                    else if (readingLayout)
                    {
                        //List to store the row. read in from the layout section of the file.
                        List<int> row = new List<int>();
                        string[] cells = line.Split(' ');
                        //double check theres no null character in cells array.
                        foreach(string c in cells)
                        {
                            if (!string.IsNullOrEmpty(c))
                                row.Add(int.Parse(c)); 
                        }
                        //Add the row to the temp layout.
                        tempLayout.Add(row);
                    }


                }
            }
            //Grab the width and height of the temp layout based on the size of its contents.
            int width = tempLayout[0].Count;
            int height = tempLayout.Count;
            //Create a new tile layer.
            tileLayer = new TileLayer(width, height);
            //Copy the information in the temp layout to the map of the layer.
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    tileLayer.SetCellIndex(x, y, tempLayout[y][x]);
                }
            }
            String[] textureNamesArray = textureNames.ToArray();
            tileLayer.LoadTileTextures(content, textureNamesArray);


            return tileLayer;
        }

        public void LoadTileTextures(ContentManager content, params string[] textureNames)
        {
            foreach (string textureName in textureNames)
            {
                Texture2D texture;
                texture = content.Load<Texture2D>(textureName);
                tileTextures.Add(texture);
            }
        }

        public void AddTexture(Texture2D t)
        {
            this.tileTextures.Add(t);
        }

        public int GetCellIndex(Point point)
        {
            return map[point.Y, point.X];
        }


        public void SetCellIndex(Point point, int cellIndex)
        {
            map[point.Y, point.X] = cellIndex;
        }


        public void SetCellIndex(int x, int y, int cellIndex)
        {
            map[y, x] = cellIndex;
        }
    
        //Draw method to draw the tile layer without culling.
        //Do not use unless you dont need the extra optimization from culling or have a small map.
        public void Draw(SpriteBatch batch, Camera camera)
        {
            batch.Begin(SpriteSortMode.Texture, null,null,null,null,null,camera.TransformMatrix);
            //get the width of the map 2d Array
            int tileMapWidth = map.GetLength(1);
            int tileMapHeight = map.GetLength(0);

            for (int x = 0; x < tileMapWidth; x++)
            {
                for (int y = 0; y < tileMapHeight; y++)
                {
                    //Retrive texture index from the tileMap.
                    int textureIndex = map[y, x];
                    if (textureIndex == -1)
                        continue;
                    
                    //retrive texture from the texture list.
                    Texture2D texture = tileTextures[textureIndex];

                    //Set the drawing position of the sprite based on the x and y values and the width and height of the tile.
                    //Set the position of the rectangle based on where the camera position hence why we subtract.
                    Rectangle drawPos = new Rectangle(
                        x * Engine.TileWidth ,
                        y * Engine.TileHeight,
                        Engine.TileWidth,
                        Engine.TileHeight);


                    batch.Draw(texture, drawPos, Color.White * alpha);
                }
            }

            batch.End();
        }
        //gfg
        //Draw method that will draw within a min and max bounds. Used for camera culling.
        public void Draw(SpriteBatch batch, Camera camera, Point min, Point max)
        {
            batch.Begin(SpriteSortMode.Texture, null, null, null, null, null, camera.TransformMatrix);
            //get the width of the map 2d Array
            int tileMapWidth = map.GetLength(1);
            int tileMapHeight = map.GetLength(0);
            //The min X and Y coordinate must be above 0.
            min.X = (int)Math.Max(min.X, 0);
            min.Y = (int)Math.Max(min.Y, 0);
            //Max X and Y coordinate must be below (width,height) 
            max.X = (int)Math.Min(max.X, tileMapWidth);
            max.Y = (int)Math.Min(max.Y, tileMapHeight);

            //With in the bounds of min and X read the tilelayer and draw the appropriate textures.
            for (int x = min.X; x < max.X; x++)
            {
                for (int y = min.Y; y < max.Y; y++)
                {
                    //Retrive texture index from the tileMap.
                    int textureIndex = map[y, x];
                    if (textureIndex == -1)
                        continue;

                    //retrive texture from the texture list.
                    Texture2D texture = tileTextures[textureIndex];

                    //Set the drawing position of the sprite based on the x and y values and the width and height of the tile.
                    //Set the position of the rectangle based on where the camera position hence why we subtract.
                    Rectangle drawPos = new Rectangle(
                        x * Engine.TileWidth,
                        y * Engine.TileHeight,
                        Engine.TileWidth,
                        Engine.TileHeight);


                    batch.Draw(texture, drawPos, Color.White * alpha);
                }
            }

            batch.End();
        }
    }


}
