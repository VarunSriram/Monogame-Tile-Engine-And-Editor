﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace TileEngine
{
    public class Camera
    {
        //Float to store the camera's movement speed;
       
        bool useGamePad = true;
        //Store the camera position as a vector2.
        public Vector2 Position = Vector2.Zero;
        public float zoom = 1;
        public float ZoomLevel
        {
            get { return zoom; }
            set { zoom = value; }
        }

        //Matrix to store the Transform data of the camera.
        public Matrix TransformMatrix
        {
            get
            {
                //Return position data in the form of a translation in the matrix.
                return Matrix.CreateTranslation(new Vector3(-Position, 0f)) * Matrix.CreateScale(zoom,zoom,1);
            }
        }

        public void ClampToArea(int width, int height)
        {
            if (Position.X > width*zoom)
                Position.X = width*zoom;

            if (Position.Y > height*zoom)
                Position.Y = height*zoom;

            if (Position.X < 0)
                Position.X = 0;
            if (Position.Y < 0)
                Position.Y = 0;
        }

        public void lockToTarget(AnimatedSprite sprite, int screenWidth, int screenHeight)
        {
            Position.X = sprite.Position.X +
                               (sprite.CurrentAnimation.CurrentRect.Width) -
                               (screenWidth / 2);
            Position.Y = sprite.Position.Y +
                                (sprite.CurrentAnimation.CurrentRect.Height) -
                                (screenHeight / 2);
        }


    }
}
