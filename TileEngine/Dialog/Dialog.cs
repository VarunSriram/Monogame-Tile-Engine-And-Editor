﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileEngine
{
    public class Dialog : DrawableGameComponent
    {
        //string to represent the text of the dialog.
        public string Text = string.Empty;

        public Conversation Conversation = null;

        public NPC Npc = null;

        public bool FirstFrame = true;

        public Rectangle Area = new Rectangle(0, 0, 900, 400);

        //Content manager to load the fonts for the text.
        ContentManager content;
        //Sprite batch needed to draw the text to screen.
        SpriteBatch spriteBatch;
        //sprite font to define the font of hte text.
        SpriteFont spriteFont;
        //Texture needed for the texts background.
        Texture2D background;

        //int to store the current index number of the handler.
        int currentHandler = 0;
        //KeyboardState to store the state of the keyboard last frame.
        KeyboardState lastState;


        //Constructor of the Dialog class.
        public Dialog(Game game, ContentManager content) 
            :base(game)
        {
            this.content = content;
        }

        public override void Initialize()
        {
            lastState = Keyboard.GetState();
            base.Initialize();
        }

        protected override void LoadContent()
        {

            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteFont = content.Load<SpriteFont>("Fonts/Arial");
            background = new Texture2D(GraphicsDevice,1,1,false,SurfaceFormat.Color);
            background.SetData<Color>(new Color[] { Color.White });
        }


        public override void Update(GameTime gameTime)
        {
            
            if (Conversation != null && Npc != null)
            {
                if (!FirstFrame)
                {
                    //If up is pressed in the current and released last frame...
                    if (InputHelper.IsNewPress(Keys.Up))
                    {
                        //Reduce the currentHandler
                        currentHandler--;
                        //If the current handler is 0 go back to the handlers count -1 to prevent negative index.
                        if (currentHandler < 0)
                        {
                            currentHandler = Conversation.Handlers.Count - 1;
                        }
                    }

                    //If down key is pressed now and was released last frame...
                    if (InputHelper.IsNewPress(Keys.Down))

                    {
                        //increase the current handler while clamping it to the handler list's count.
                        currentHandler = (currentHandler + 1) % Conversation.Handlers.Count;
                    }

                    //If spece key is pressed now and was released last frame...
                    if (InputHelper.IsNewPress(Keys.Space))
                    {
                        Conversation.Handlers[currentHandler].Invoke(Npc);
                        currentHandler = 0;
                    }
                }
                else
                   FirstFrame = false;
                

            }

           
           
        }

        public override void Draw(GameTime gameTime)
        {
            
            Rectangle dest = new Rectangle(
                GraphicsDevice.Viewport.Width / 2 - Area.Width / 2,
                GraphicsDevice.Viewport.Height / 2 - Area.Height / 2,
                Area.Width,
                Area.Height);
           
            spriteBatch.Begin();
            spriteBatch.Draw(background, dest, new Color(0,0,0,100));
            spriteBatch.End();
            if(Conversation == null)
            {
                return;
            }
            spriteBatch.Begin();
            string fullText = WrapText(Conversation.Text);
            spriteBatch.DrawString(spriteFont, fullText, new Vector2(dest.X, dest.Y),Color.White);

            int lineHeight = (int) spriteFont.MeasureString(" ").Y;
            int startingHeight = (int)spriteFont.MeasureString(fullText).Y + lineHeight;

            for(int i = 0; i < Conversation.Handlers.Count; i++)
            {
                string caption = Conversation.Handlers[i].Caption;

                Color color = (i == currentHandler) ? Color.Yellow : Color.White;
                spriteBatch.DrawString(spriteFont, caption, new Vector2(dest.X, dest.Y + startingHeight+ (i*lineHeight)),color);
            }
           

            spriteBatch.End();
            

        }
        //Method to wrap the text.
        private string WrapText(string text)
        {
            string[] words = text.Split(' ');
            StringBuilder sb = new StringBuilder();

            float lineWidth = 0f;
            float spaceWidth = spriteFont.MeasureString(" ").X;
            foreach(string word in words)
            {
                Vector2 size = spriteFont.MeasureString(word);
                if(lineWidth + size.X < Area.Width)
                {
                    sb.Append(word + " ");
                    lineWidth += size.X+spaceWidth;
                }
                else
                {
                    sb.Append("\n" + word + " ");
                    lineWidth = size.X + spaceWidth;
                }
            }

            return sb.ToString();

        }

       
    }
}
