﻿using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Collections.ObjectModel;
using System.Collections.Generic;
namespace TileEngine
{
    //Class to parse a XML conversation script.
    public class Script
    {
        //A dictionary to catalog our conversations read in from the XML file.
        Dictionary<string, Conversation> conversations = new Dictionary<string, Conversation>();

        //Accesssor for a conversation given a name.
        public Conversation this[string name]
        {
            get { return conversations[name]; }
        }

        public Script(params Conversation[] newConversations)
        {
            foreach (Conversation c in newConversations)
            {
                conversations.Add(c.Name, c);
            }
        }
    }

    //Class for the conversations outlined in the XML
    public class Conversation
    {
        //String for the name of the conversation as shown in the XML
        string name;
        //String for the text text
        string text;
        //Collection of conversation handlers of the conversation.
        public Collection<ConversationHandler> Handlers = new Collection<ConversationHandler>();
        //Accessor for the name.
        public string Name
        {
            get { return name; }
        }

        public string Text
        {
            get { return text; }
        }

        //Constructor for the Conversation.
        public Conversation(string name, string text, params ConversationHandler[] newHandlers)
        {
            this.name = name;
            this.text = text;
            //For each conversation handler add it to hte collection.
            foreach (ConversationHandler c in newHandlers)
            {
                Handlers.Add(c);
            }
        }

    }
    //Class for the handler of conversations outlined in the XML
    public class ConversationHandler
    {
        //String to store the caption of the handler
        string caption;
        ConversationHandlerAction[] actions;


        //Accessor for the caption.
        public string Caption
        {
            get { return caption; }
        }

        //Constructor for the Conversation Handler.
        public ConversationHandler(string caption, params ConversationHandlerAction[] actions)
        {
            this.caption = caption;                      
            this.actions =  actions;
            
        }

        public void Invoke(NPC npc)
        {
            foreach(ConversationHandlerAction action in actions) 
                action.Invoke(npc);
        }



    }

    public class ConversationHandlerAction
    {
        //Using reflection to tell what method the handler will invoke.
        MethodInfo method;
        //array of objects for the parameters for the method.
        object[] parameters;

        public ConversationHandlerAction(string methodName, object[] parameters)
        {
            method = typeof(NPC).GetMethod(methodName);
            this.parameters = parameters;
            
        }

        public void Invoke(NPC npc)
        {
            method.Invoke(npc, parameters);
        }
    }
}
