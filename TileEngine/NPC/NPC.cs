﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
namespace TileEngine
{
    public class NPC : AnimatedSprite
    {
        //NPC use the script class to talk.
        Dialog dialog;
        Script script;
        //float to store the minium radius the player has to be to speak to the NPC.
        float speakingRadius = 50;
        public AnimatedSprite Target;
        bool followingTarget = false;
        //Accessor for the speaking radius.
        public float SpeakingRadius
        {
            get { return speakingRadius; }
            set { speakingRadius = (float)Math.Max(value, CollisionRadius); }
        }

        public override void Update(GameTime gameTime)
        {
            if (Target != null && followingTarget)
            {
                Position = Target .Center + new Vector2(Target .CollisionRadius + CollisionRadius, 0f);

            }
            base.Update(gameTime);
        }


        public NPC(Texture2D texture, Dialog dialog, Script script)
            : base(texture)
        {
            this.dialog = dialog;
            this.script = script;
        }

        public bool InSpeakingRange(AnimatedSprite sprite)
        {
            Vector2 d = Origin - sprite.Origin;
            return (d.Length() < SpeakingRadius);
        }

        public void StartConversation(string conversationName)
        {
            if(script == null || dialog == null)
            {
                return;
            }
            dialog.Enabled = true;
            dialog.Visible = true;
            dialog.Npc = this;
            dialog.FirstFrame = true;
            dialog.Conversation = script[conversationName];
        }

        public void EndConversation()
        {
            if (script == null || dialog == null)
            {
                return;
            }
            dialog.Enabled = false;
            dialog.Visible = false;

        }

        public void StartFollowing()
        {
            followingTarget = true;
        }

        public void StopFollowing()
        {
            followingTarget = false;
        }
    }
}
