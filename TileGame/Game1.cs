﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using TileEngine;
namespace TileGame
{//
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        
        TileMap tileMap = new TileMap();


        //Sprite for our player
        AnimatedSprite sprite;
        //List of sprites for our npc's.
        List<NPC> npcs = new List<NPC>();
        //List of sprites that require depth.
        List<AnimatedSprite> renderList = new List<AnimatedSprite>();
        //Comparison to sort our renderlist.
        Comparison<AnimatedSprite> renderSort = new Comparison<AnimatedSprite>(renderSpriteCompare);

       

        Dialog dialog;

        //Camera declared to view our map to screen.
        Camera camera = new Camera();
       

        bool useGamePad = true;

        static int renderSpriteCompare(AnimatedSprite a, AnimatedSprite b)
        {
            return a.Origin.Y.CompareTo(b.Origin.Y);
        }

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = 1280,
                PreferredBackBufferHeight = 720
            };

           // graphics.SynchronizeWithVerticalRetrace = false;
           // base.IsFixedTimeStep = false;

            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            dialog = new Dialog(this, Content);
            dialog.Enabled = false;
            dialog.Visible = false;
            this.Components.Add(dialog);
           



            base.Initialize();

            // TODO: Add your initialization logic here
           // tileMap.Layers.Add(tileLayer);
            //Create a frame animation for each animation for player and npcs.
            FrameAnimation up = new FrameAnimation(2, 32, 32, 0, 0);
            up.FramesPerSecond = 5;
            sprite.Animations.Add("Up", up);

            FrameAnimation down = new FrameAnimation(2, 32, 32, 64, 0);
            down.FramesPerSecond = 5;
            sprite.Animations.Add("Down", down);

            FrameAnimation left = new FrameAnimation(2, 32, 32, 128, 0);
            left.FramesPerSecond = 5;
            sprite.Animations.Add("Left", left);

            FrameAnimation right = new FrameAnimation(2, 32, 32, 192, 0);
            right.FramesPerSecond = 5;
            sprite.Animations.Add("Right", right);

            //Random class for randomly choosing positions and animations.
            Random rand = new Random();
            //For each animated sprite in the npcs list add their animations choose a random default animation and place them in a random
            //location.
            foreach(AnimatedSprite s in npcs)
            {
                s.OriginOffset = new Vector2(16, 32);
                s.Animations.Add("Up", (FrameAnimation)up.Clone());
                s.Animations.Add("Down", (FrameAnimation)down.Clone());
                s.Animations.Add("Left", (FrameAnimation)left.Clone());
                s.Animations.Add("Right", (FrameAnimation)right.Clone());

                int animation = rand.Next(3);

                switch (animation)
                {
                    case 0:
                        s.CurrentAnimationName = "Up";
                        break;

                    case 1:
                        s.CurrentAnimationName = "Down";
                        break;

                    case 2:
                        s.CurrentAnimationName = "Left";
                        break;
                    case 3:
                        s.CurrentAnimationName = "Right";
                        break;

                }

                s.Position = new Vector2(
                    rand.Next(400),
                    rand.Next(400));

                renderList.Add(s);
            }

           

            sprite.CurrentAnimationName = "Down";
            renderList.Add(sprite);
            
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            tileMap.Layers.Add(Content.Load<TileLayer>("Layers/Layer2"));
            // tileLayer = TileLayer.FromFile(Content, "Content/Layers/Layer1.layer");

            //Load Collision layer.


            //  tileMap.CollisionLayer = CollisionLayer.FromFile(Content, "Content/Layers/Collision.Layer");
            tileMap.CollisionLayer = Content.Load<CollisionLayer>("Layers/Collision");

            sprite = new AnimatedSprite(Content.Load<Texture2D>("Sprites/amg1"));
            sprite.OriginOffset = new Vector2(16, 32);

          
           
            NPC npc = new NPC(Content.Load<Texture2D>("Sprites/amg1"), dialog, Content.Load<Script>("Scripts/NPC1"));
            npc.Target = sprite;
            npcs.Add(npc);
            npcs.Add(new NPC(Content.Load<Texture2D>("Sprites/npc3"), null, null));
            npcs.Add(new NPC(Content.Load<Texture2D>("Sprites/avt2"), null, null));
            npcs.Add(new NPC(Content.Load<Texture2D>("Sprites/chr1"), null, null));
            npcs.Add(new NPC(Content.Load<Texture2D>("Sprites/npc4"), null, null));
            npcs.Add(new NPC(Content.Load<Texture2D>("Sprites/npc6"), null, null));

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            
          
           

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            useGamePad = GamePad.GetState(PlayerIndex.One).IsConnected;
            //Vector2 to describe the motion of the camera.
            Vector2 motion = Vector2.Zero;
            GamePadState gamepadState = GamePad.GetState(PlayerIndex.One);
            //Update the Input State.
            InputHelper.Update();


            if (!dialog.Enabled)
            {
                          
                if (useGamePad)
                {
                    
                    motion = new Vector2(gamepadState.ThumbSticks.Left.X, -gamepadState.ThumbSticks.Left.Y);
                }
                else
                {

                   
                    //Store the state of the gamepad everytime the game updates.

                    //Checks to see if the directional buttons on the keyboard are pressed.
                    if (InputHelper.IsKeyDown(Keys.Up))
                        motion.Y--;
                    if (InputHelper.IsKeyDown(Keys.Down))
                        motion.Y++;
                    if (InputHelper.IsKeyDown(Keys.Right))
                        motion.X++;
                    if (InputHelper.IsKeyDown(Keys.Left))
                        motion.X--;
                }

                //You cannot normalize a vector that has a zero distance therefore before normalizing check it is not zero.
                if (motion != Vector2.Zero)
                {
                    //Normalize the Vector to ensure no matter what direction we are going it will go at that same rate.
                    if (!useGamePad)
                    {
                        motion.Normalize();
                    }

                    motion = CheckCollisionForMotion(motion, sprite);


                    sprite.Position += motion * sprite.Speed * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                    UpdateSpriteAnimation(motion);
                    sprite.IsAnimating = true;
                    //checking for unwalkable areas of the map.
                    CheckForUnwalkableTiles(sprite);

                }
                else
                {
                    sprite.IsAnimating = false;
                }


                sprite.ClampToArea((int)tileMap.GetWidthInPixels(), (int)tileMap.GetHeightInPixels());

                sprite.Update(gameTime);

                foreach (AnimatedSprite s in npcs)
                {
                    s.Update(gameTime);
                    if (AnimatedSprite.AreColliding(sprite, s))
                    {
                        Vector2 d = Vector2.Normalize(s.Origin - sprite.Origin);
                        sprite.Position = s.Position - (d * (sprite.CollisionRadius + s.CollisionRadius));

                    }

                    if( s is NPC)
                    {
                        NPC character = s as NPC;
                        if (character.InSpeakingRange(sprite) && !dialog.Enabled && InputHelper.IsNewPress(Keys.Space))
                        {
                            character.StartConversation("Greetings");
                        }
                    }
                }

                //Size of the screen needs to be taken into account what our camera can and what the boundaries of our camera movement is.
                int screenWidth = GraphicsDevice.Viewport.Width;
                int screenHeight = GraphicsDevice.Viewport.Height;


                camera.lockToTarget(sprite, screenWidth, screenHeight);
                camera.ClampToArea((int)tileMap.GetWidthInPixels() - screenWidth - 1, (int)tileMap.GetHeightInPixels() - screenHeight - 1);
            }

            base.Update(gameTime);
        }

        private void CheckForUnwalkableTiles(AnimatedSprite sprite)
        {
            Point spriteCell = Engine.ConvertPositionToCell(sprite.Center);
            Point? upLeft = null, upRight = null, up = null, left = null, right = null, downleft = null, down = null, downRight = null;


            left = right = up = down = new Point();


            if (spriteCell.Y > 0)
                up = new Point(spriteCell.X, spriteCell.Y - 1);
            if (spriteCell.Y < tileMap.CollisionLayer.Height - 1)
                down = new Point(spriteCell.X, spriteCell.Y + 1);
            if (spriteCell.X > 0)
                left = new Point(spriteCell.X - 1, spriteCell.Y);
            if(spriteCell.X < tileMap.CollisionLayer.Width -1)
                right = new Point(spriteCell.X + 1 , spriteCell.Y);


            if(spriteCell.X > 0 && spriteCell.Y > 0)
                upLeft = new Point(spriteCell.X - 1, spriteCell.Y - 1);
            if (spriteCell.X < tileMap.CollisionLayer.Width-1 && spriteCell.Y > 0)
                upRight = new Point(spriteCell.X + 1, spriteCell.Y - 1);
            if (spriteCell.X > 0 && spriteCell.Y < tileMap.CollisionLayer.Height-1)
                downleft = new Point(spriteCell.X - 1, spriteCell.Y + 1);
            if (spriteCell.X < tileMap.CollisionLayer.Width - 1 && spriteCell.Y > tileMap.CollisionLayer.Height - 1)
                downRight = new Point(spriteCell.X + 1, spriteCell.Y + 1);


            if (up != null && tileMap.CollisionLayer.GetCellIndex(up.Value) == 1)
            {

                Rectangle cellRect = Engine.CreateRectForCell(up.Value);
                Rectangle spriteRect = sprite.Bounds;

                if (cellRect.Intersects(spriteRect))
                {
                    sprite.Position.Y = spriteCell.Y * Engine.TileHeight;
                }
            }

            if (down != null && tileMap.CollisionLayer.GetCellIndex(down.Value) == 1)
            {

                Rectangle cellRect = Engine.CreateRectForCell(down.Value);
                Rectangle spriteRect = sprite.Bounds;

                if (cellRect.Intersects(spriteRect))
                {
                    sprite.Position.Y = down.Value.Y * Engine.TileHeight - sprite.Bounds.Height;
                }
            }

            if (left != null && tileMap.CollisionLayer.GetCellIndex(left.Value) == 1)
            {

                Rectangle cellRect = Engine.CreateRectForCell(left.Value);
                Rectangle spriteRect = sprite.Bounds;

                if (cellRect.Intersects(spriteRect))
                {
                    sprite.Position.X = spriteCell.X * Engine.TileWidth;
                }
            }

            if (right != null && tileMap.CollisionLayer.GetCellIndex(right.Value) == 1)
            {

                Rectangle cellRect = Engine.CreateRectForCell(right.Value);
                Rectangle spriteRect = sprite.Bounds;

                if (cellRect.Intersects(spriteRect))
                {
                    sprite.Position.X = right.Value.X * Engine.TileWidth - sprite.Bounds.Width;
                }
            }

            if(upLeft != null && tileMap.CollisionLayer.GetCellIndex(upLeft.Value) == 1)
            {
                Rectangle cellRect = Engine.CreateRectForCell(upLeft.Value);
                Rectangle spriteRect = sprite.Bounds;

                if (cellRect.Intersects(spriteRect))
                {
                    sprite.Position.Y = spriteCell.Y * Engine.TileHeight;
                    sprite.Position.X = spriteCell.X * Engine.TileWidth;
                }
            }

            if (upRight != null && tileMap.CollisionLayer.GetCellIndex(upRight.Value) == 1)
            {
                Rectangle cellRect = Engine.CreateRectForCell(upRight.Value);
                Rectangle spriteRect = sprite.Bounds;

                if (cellRect.Intersects(spriteRect))
                {
                    sprite.Position.Y = spriteCell.Y * Engine.TileHeight;
                    sprite.Position.X = right.Value.X * Engine.TileWidth - sprite.Bounds.Width;
                }
            }

            if (downleft != null && tileMap.CollisionLayer.GetCellIndex(downleft.Value) == 1)
            {
                Rectangle cellRect = Engine.CreateRectForCell(downleft.Value);
                Rectangle spriteRect = sprite.Bounds;

                if (cellRect.Intersects(spriteRect))
                {
                    sprite.Position.Y = down.Value.Y * Engine.TileHeight - sprite.Bounds.Height;
                    sprite.Position.X = spriteCell.X * Engine.TileWidth;
                }
            }

            if (downRight != null && tileMap.CollisionLayer.GetCellIndex(downRight.Value) == 1)
            {
                Rectangle cellRect = Engine.CreateRectForCell(downRight.Value);
                Rectangle spriteRect = sprite.Bounds;

                if (cellRect.Intersects(spriteRect))
                {
                    sprite.Position.Y = down.Value.Y * Engine.TileHeight - sprite.Bounds.Height;
                    sprite.Position.X = right.Value.X * Engine.TileWidth - sprite.Bounds.Width;
                }
            }






        }

        private void UpdateSpriteAnimation(Vector2 motion)
        {
            //Get the angle of motion of the sprite.
            float motionAngle = (float)Math.Atan2(motion.Y, motion.X);

            //Based on the angle of motion it chooses the proper animation.
            //NOTE: uncomment the motion reassignment in each if statement to restrict motion up down left right.
            if (motionAngle >= -MathHelper.PiOver4 &&
                motionAngle <= MathHelper.PiOver4)
            {
                sprite.CurrentAnimationName = "Right";
                //  motion = new Vector2(1f, 0f);
            }
            else if (motionAngle >= MathHelper.PiOver4 &&
                     motionAngle <= 3f * MathHelper.PiOver4)
            {
                sprite.CurrentAnimationName = "Down";
                // motion = new Vector2(0f, 1f);
            }
            else if (motionAngle <= -MathHelper.PiOver4 &&
                     motionAngle >= -3f * MathHelper.PiOver4)
            {
                sprite.CurrentAnimationName = "Up";
                // motion = new Vector2(0f, -1f);
            }
            else
            {
                sprite.CurrentAnimationName = "Left";
                //  motion = new Vector2(-1f, 0f);
            }
        }

        private Vector2 CheckCollisionForMotion(Vector2 motion,AnimatedSprite sprite)
        {
            Point cell = Engine.ConvertPositionToCell(sprite.Origin);
            int colIndex = tileMap.CollisionLayer.GetCellIndex(cell);
            if(colIndex == 2)
            {
                return motion *.2f;
            }

            return motion;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            //Draw using our tileLayer's draw method.
            
            tileMap.Draw(spriteBatch, camera);

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, camera.TransformMatrix);
            renderList.Sort(renderSort);
            foreach(AnimatedSprite s in renderList)
            {
                s.Draw(spriteBatch);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
