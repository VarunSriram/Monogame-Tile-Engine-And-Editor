﻿using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using System;

namespace TileContent
{
    [ContentTypeWriter]
    public class CollisionLayerWriter : ContentTypeWriter<CollisionLayerContent>
    {
        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "TileEngine.CollisionLayerReader, TileEngine";
        }

        protected override void Write(ContentWriter output, CollisionLayerContent value)
        {
            int height = value.Layout.GetLength(0);
            int width = value.Layout[0].GetLength(0);

            output.Write(height);
            output.Write(width);
            for(int y = 0; y < height; y++)
            {
                for(int x = 0; x < width; x++)
                {
                    output.Write(value.Layout[y][x]);
                }
            }

        }
    }
}
