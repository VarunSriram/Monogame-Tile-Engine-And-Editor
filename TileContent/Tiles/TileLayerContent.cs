﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;

namespace TileContent
{
    public class CollisionLayerContent
    {
        public int[][] Layout;
    }



    public class TileLayerContent
    {
        //Colleciton of TileLayerTexture Contents to store texture information
        public Collection<TileLayerTextureContent> Textures = new Collection<TileLayerTextureContent>();

        public Collection<TileLayerPropertyContent> Properties = new Collection<TileLayerPropertyContent>();

        public int[][] Layout; 


    }

    public class TileLayerTextureContent
    {
        public ExternalReference<TextureContent> Texture;
        public int Index;
    }

    public class TileLayerPropertyContent
    {
        public string Name;
        public string Value;

    }
}
