﻿using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using System;

namespace TileContent
{
    [ContentTypeWriter]
    public class TileLayerWriter : ContentTypeWriter<TileLayerContent>
    {
        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "TileEngine.TileLayerReader, TileEngine";
        }

        protected override void Write(ContentWriter output, TileLayerContent value)
        {
            int height = value.Layout.GetLength(0);
            int width = value.Layout[0].GetLength(0);

            output.Write(height);
            output.Write(width);
            for(int y = 0; y < height; y++)
            {
                for(int x = 0; x < width; x++)
                {
                    output.Write(value.Layout[y][x]);
                }
            }

            output.Write(value.Textures.Count);
            foreach (TileLayerTextureContent textContent in value.Textures)
            {
                output.WriteExternalReference<TextureContent>(textContent.Texture);
                output.Write(textContent.Index);
            }


            output.Write(value.Properties.Count);

            foreach(TileLayerPropertyContent propContent in value.Properties)
            {
                output.Write(propContent.Name);
                output.Write(propContent.Value);
            }
            

        }
    }
}
