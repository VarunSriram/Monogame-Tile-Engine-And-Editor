﻿using System;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline;
using System.Xml;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;

namespace TileContent
{
    /// <summary>
    /// This class will be instantiated by the XNA Framework Content Pipeline
    /// to apply custom processing to content data, converting an object of
    /// type TInput to TOutput. The input and output types may be the same if
    /// the processor wishes to alter data without changing its type.
    ///
    /// This should be part of a Content Pipeline Extension Library project.
    ///
    /// TODO: change the ContentProcessor attribute to specify the correct
    /// display name for this processor.
    /// </summary>
    [ContentProcessor(DisplayName = "Collision Layer Processor")]
    public class CollisionLayerProcessor : ContentProcessor<XmlDocument, CollisionLayerContent>
    {
        public override CollisionLayerContent Process(XmlDocument input, ContentProcessorContext context)
        {
            //Create a new TileyLayerContent instance to store tile layer inforamtion.
            CollisionLayerContent layer = new CollisionLayerContent();
            //For each child XML node in the Document Element  in the input Xml Document.
            foreach (XmlNode node in input.DocumentElement.ChildNodes)
            {
              
                
                if (node.Name == "Layout")
                {
                    //get the width and height of the layout by parsing whats in the width and height tags in the XMl doc.
                    int width = int.Parse(node.Attributes["Width"].Value);
                    int height = int.Parse(node.Attributes["Height"].Value);
                    //initialize the 2D array.

                    //layer.Layout = new int[height][];
                    int[][] readLayout = new int[height][];
                    for (int i = 0; i < height; i++)
                    {
                        readLayout[i] = new int[width];
                    }

                   
                    //read the layout as a large string.
                    string layout = node.InnerText;
                    //split this string by new line character to get each row in the layout.
                    string[] lines = layout.Split('\r', '\n');
                   
                    int row = 0;

                    foreach (string line in lines)
                    {
                        string realLine = line.Trim();
                        if (string.IsNullOrEmpty(realLine))
                        {
                            continue;
                        }

                        string[] cells = realLine.Split(' ');
                       
                        for (int x = 0; x < width; x++)
                        {
                            int cellIndex = int.Parse(cells[x]);
                            readLayout[row][x] = cellIndex;
                        }

                        row++;

                    }
                    layer.Layout = readLayout;
                }
            }

            return layer;
        }
    }
}
