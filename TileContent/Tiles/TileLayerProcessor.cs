﻿using System;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline;
using System.Xml;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;

namespace TileContent
{
    /// <summary>
    /// This class will be instantiated by the XNA Framework Content Pipeline
    /// to apply custom processing to content data, converting an object of
    /// type TInput to TOutput. The input and output types may be the same if
    /// the processor wishes to alter data without changing its type.
    ///
    /// This should be part of a Content Pipeline Extension Library project.
    ///
    /// TODO: change the ContentProcessor attribute to specify the correct
    /// display name for this processor.
    /// </summary>
    [ContentProcessor(DisplayName = "Tile Layer Processor")]
    public class TileLayerProcessor : ContentProcessor<XmlDocument, TileLayerContent>
    {
        public override TileLayerContent Process(XmlDocument input, ContentProcessorContext context)
        {
            //Create a new TileyLayerContent instance to store tile layer inforamtion.
            TileLayerContent layer = new TileLayerContent();
            //For each child XML node in the Document Element  in the input Xml Document.
            foreach (XmlNode node in input.DocumentElement.ChildNodes)
            {
                //If we are accessing the Textures Node.
                if (node.Name == "Textures")
                {

                    //For each TextureNode in the child of node textures.
                    foreach (XmlNode textureNodes in node.ChildNodes)
                    {
                        //Grab the asset Name/Directory
                        string file = textureNodes.Attributes["Asset"].Value;
                        //Grab the texture index from the index node.
                        int index = int.Parse(textureNodes.Attributes["ID"].Value);
                        //Create A new Texture Content class (see TileLayerContent.cs)
                        TileLayerTextureContent textureContent = new TileLayerTextureContent();
                        //OpaqueDataDictionary to define certain parameters about the texture we are trying to build.
                        OpaqueDataDictionary textureData = new OpaqueDataDictionary();
                        //Add a generate mip maps key to the dictionary with a value true. This is done so we can generate mipmaps when we build the texture.
                        textureData.Add("GenerateMipmaps", true);

                        //Set the texture by building the asset using the Texture processor. 
                        textureContent.Texture = context.BuildAsset<TextureContent, TextureContent>(new ExternalReference<TextureContent>(file), "TextureProcessor",textureData,"TextureImporter",file);
                        //Set the index by the one grabbed in the XML doc
                        textureContent.Index = index;
                        //add the texturecontent to the TileLayerContent.
                        layer.Textures.Add(textureContent);
                    }
                }
                else if (node.Name == "Properties")
                {
                    //For each property Node in the properties node
                    foreach (XmlNode propertyNode in node.ChildNodes)
                    {
                        //Create a new property content instance
                        TileLayerPropertyContent propContent = new TileLayerPropertyContent();
                        //set the name to the one read in from the one property node.
                        propContent.Name = propertyNode.Name;
                        //Set the value to the inner text of the node.
                        propContent.Value = propertyNode.InnerText;
                        //add the property to the layer.
                        layer.Properties.Add(propContent);
                    }
                    //dsds
                }
                
                else if (node.Name == "Layout")
                {
                    //get the width and height of the layout by parsing whats in the width and height tags in the XMl doc.
                    int width = int.Parse(node.Attributes["Width"].Value);
                    int height = int.Parse(node.Attributes["Height"].Value);
                    //initialize the 2D array.

                    //layer.Layout = new int[height][];
                    int[][] readLayout = new int[height][];
                    for (int i = 0; i < height; i++)
                    {
                        readLayout[i] = new int[width];
                    }

                   
                    //read the layout as a large string.
                    string layout = node.InnerText;
                    //split this string by new line character to get each row in the layout.
                    string[] lines = layout.Split('\r', '\n');
                   
                    int row = 0;

                    foreach (string line in lines)
                    {
                        string realLine = line.Trim();
                        if (string.IsNullOrEmpty(realLine))
                        {
                            continue;
                        }

                        string[] cells = realLine.Split(' ');
                       
                        for (int x = 0; x < width; x++)
                        {
                            int cellIndex = int.Parse(cells[x]);
                            readLayout[row][x] = cellIndex;
                        }

                        row++;

                    }
                    layer.Layout = readLayout;
                }
            }

            return layer;
        }
    }
}
