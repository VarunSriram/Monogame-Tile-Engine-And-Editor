﻿using System;
using Microsoft.Xna.Framework.Content.Pipeline;
using System.Xml;

namespace TileContent
{
    /// <summary>
    /// This class will be instantiated by the XNA Framework Content Pipeline
    /// to apply custom processing to content data, converting an object of
    /// type TInput to TOutput. The input and output types may be the same if
    /// the processor wishes to alter data without changing its type.
    ///
    /// This should be part of a Content Pipeline Extension Library project.
    ///
    /// TODO: change the ContentProcessor attribute to specify the correct
    /// display name for this processor.
    /// </summary>
    [ContentProcessor(DisplayName = "NPC Script Processor")]
    public class ScriptProcessor : ContentProcessor<XmlDocument, ScriptContent>
    {
        public override ScriptContent Process(XmlDocument input, ContentProcessorContext context)
        {
            //Declare a new ScriptContent Class to store data from the XML file.
            ScriptContent script = new ScriptContent();
            //Get list of nodes that have the tag Conversation.
            XmlNodeList conversations = input.GetElementsByTagName("Conversation");
            //For each XML node create a new conversationContent instance and fill it with info from the node.
            foreach(XmlNode node in conversations)
            {
                ConversationContent c = new ConversationContent();
                c.Name = node.Attributes["Name"].Value;
                c.Text = node.FirstChild.InnerText;
                //For each handler in the Conversation Content.
                foreach(XmlNode handlerNode in node.LastChild.ChildNodes)
                {
                    //Create a new instance of ConversationHandler Content.
                    ConversationHandlerContent h = new ConversationHandlerContent();
                    //Caption is equal to the content in the XML node attribute "Content".
                    h.Caption = handlerNode.Attributes["Caption"].Value;
                    //Action is equal to the content in the XML node Attribute "Action". 
                    string action = handlerNode.Attributes["Action"].Value;

                    //Split the action by the semicolon to check for multiple methods.
                    string[] methods = action.Split(';');
                    //For each method...
                    foreach (string m in methods)
                    {
                        string trimmedMethodName = m.Trim();
                        ConversationHandlerActionContent a = new ConversationHandlerActionContent();

                        //If the action contains parameters as denoted by the : in the action
                        if (trimmedMethodName.Contains(":"))
                        {
                            //String to the left of the colon is the action of the handler and the parameters are to the right of the colon.
                            string[] actionSplit = trimmedMethodName.Split(':');
                            a.MethodName = actionSplit[0];
                            a.Parameters = (object[])actionSplit[1].Split(',');
                        }
                        else
                        {
                           a.MethodName = trimmedMethodName;
                           a.Parameters = null;
                        }
                        //Add the ConversationHandlerActionContent to the Conversation handler content.
                        h.Actions.Add(a);

                    }
                    //Add the handler to the conversation content.
                    c.Handlers.Add(h);

                }
                //add the conversation to the scriptcontent.
                script.Conversations.Add(c);
            }

            return script;
        }
       
    }
}