﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Collections.ObjectModel;
namespace TileContent
{
    //Class to represent content of an XML script file.
    public class ScriptContent
    {
        public Collection<ConversationContent> Conversations = new Collection<ConversationContent>();

        
       
    }

    //Class for the conversations outlined in the XML
    public class ConversationContent
    {
        //String for the name of the conversation as shown in the XML
        public string Name;
        //String for the text text
        public string Text;
        //Collection of conversation handlers of the conversation.
        public Collection<ConversationHandlerContent> Handlers = new Collection<ConversationHandlerContent>();
       

    }
    //Class for the handler of conversations outlined in the XML
    public class ConversationHandlerContent
    {
        //String to store the caption of the handler
       public string Caption;
       public List<ConversationHandlerActionContent> Actions = new List<ConversationHandlerActionContent>();
      


    }

    public class ConversationHandlerActionContent
    {
        public string MethodName;
        public object[] Parameters;

    }
}
